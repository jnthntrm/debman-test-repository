$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $("#contact").on("show.bs.modal", function(e) {
        console.log(e.namespace + ": opening");
        $("#contact-button").prop("disabled", true);
        $("#contact-button").removeClass('btn-outline-success');
        $("#contact-button").addClass('btn-mybutton');
    });
    $("#contact").on("shown.bs.modal", function(e) {
        console.log(e.namespace + ": open");
    });
    $("#contact").on("hide.bs.modal", function(e) {
        console.log(e.namespace + ": hiding");
        $("#contact-button").prop("disabled", false);
        $("#contact-button").removeClass('btn-mybutton');
        $("#contact-button").addClass('btn-outline-success');
    });
    $("#contact").on("hidden.bs.modal", function(e) {
        console.log(e.namespace + ": hidden");
    });
});